/*
*	Project.cpp
*
*	Created by Mateusz Urbanek on 13/03/2018.
*
*	© 2017 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*	
*	ALL RIGHTS RESERVED
*
*/

//-------------------------------------------------------------------------------------------------------------

#include <iostream>
#include "rpn-lib.h"

//-------------------------------------------------------------------------------------------------------------

int main()
{
	menu appMenu;

	appMenu.mainMenu();
	
	std::cin.get();
	return 0;
}