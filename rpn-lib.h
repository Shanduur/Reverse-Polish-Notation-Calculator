#pragma once

/*
*	Project.cpp
*
*	Created by Mateusz Urbanek on 13/03/2018.
*
*	2017 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#ifdef _WIN32
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <iostream> 
/*
 *	Header that defines the standard input/output stream objects
 *	(source: http://www.cplusplus.com/reference/iostream/).
 *
 */

#include <stack> 
/*
 *	Header that defines the stack container class.
 * 	LIFO context (last-in first-out).
 *	(source: http://www.cplusplus.com/reference/stack/).
 *
 */

#include <string>
/*
 *	This header introduces string types, character traits and a set of converting functions
 *	(source: http://www.cplusplus.com/reference/string/).
 *
 */

#include <sstream> 
/*
 *	Header providing string stream classes
 *	(source: http://www.cplusplus.com/reference/sstream/).
 *
 */

#include <fstream>
/*
 *	Header providing file stream classes
 *	(source: http://www.cplusplus.com/reference/fstream/).
 *
 */

#include <cstdlib>
/*
 *  This header defines several general purpose functions (C Standard General Utilities Library)
 *	(source: http://www.cplusplus.com/reference/cstdlib/)
 *
 */

//-------------------------------------------------------------------------------------------------------------
/*
using namespace std; - No! God, no! I will never ever use it if I don't have to.
*/
//-------------------------------------------------------------------------------------------------------------

//********* class calculations *********
class calculations
{
public:
	//
	//
	//
	// constructor
	// destructor
	void calcFromInput();
	void calcFromFile();
	calculations() { /*std::cout << "created" << std::endl;*/ };
	~calculations() { /*std::cout << "destroyed" << std::endl;*/ };

private:
	/* std::stack<data_type> stack_name;
	 * Definition of input string.
	 *
	 */
	std::stack<double> inputStack;
	std::string manualInput;
	std::fstream f_input;
	std::string fileName;

}; //	Definition of class




   //********* class menu *********
class menu
{
public:
	/*
	 * constructor
	 * destructor
	 *
	 */
	void mainMenu();
	menu() { /*std::cout << "created" << std::endl;*/ };
	~menu() { /*std::cout << "destroyed" << std::endl;*/ };

private:
	int choice;

}; //	Definition of class

   //-------------------------------------------------------------------------------------------------------------