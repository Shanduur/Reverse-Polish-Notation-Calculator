/*
*	Project.cpp
*
*	Created by Mateusz Urbanek on 13/03/2018.
*
*	2017 MATEUSZ URBANEK ( mateusz.urbanek.98@gmail.com )
*
*	GNU GPL v3.0
*
*/

#include "rpn-lib.h"


void calculations::calcFromInput()
{
	std::cout << "Aviable operations: \n"
		<< "------------------------------------\n"
		<< "|  operation       |    operator   |\n"
		<< "------------------------------------\n"
		<< "|  addition        |       +       |\n"
		<< "|  subtraction     |       -       |\n"
		<< "|  multiplication  |       *       |\n"
		<< "|  divison         |       /       |\n"
		<< "------------------------------------\n"
		<< "|  return to menu  |       x       |\n"
		<< "------------------------------------  " << std::endl;

	std::cout << "\n\n Input data:" << std::endl;



	while (true) //	Main loop in which inputs are provided and outputs are displayed
	{
		std::cin >> manualInput;

		//****** addition ******
		if (manualInput == "+")
		{
			double temporaryVariable1 = inputStack.top();								// Save last element to 1st Temporary Variable
			inputStack.pop();															// Remove last element from stack

			double temporaryVariable2 = inputStack.top();								// Save last element to 2nd Temporary Variable
			inputStack.pop();															// Remove last element from stack

			inputStack.push(temporaryVariable1 + temporaryVariable2); // Addition of variables, and pushing them to the end of stack
		}

		//****** subtraction ******
		else if (manualInput == "-")
		{
			double temporaryVariable1 = inputStack.top();								// Save last element to 1st Temporary Variable
			inputStack.pop();															// Remove last element from stack

			double temporaryVariable2 = inputStack.top();								// Save last element to 2nd Temporary Variable
			inputStack.pop();															// Remove last element from stack

			inputStack.push(temporaryVariable1 - temporaryVariable2); // Subtraction of variables, and pushing them to the end of stack
		}

		//****** multiplication ******
		else if (manualInput == "*")
		{
			double temporaryVariable1 = inputStack.top();								// Save last element to 1st Temporary Variable
			inputStack.pop();															// Remove last element from stack

			double temporaryVariable2 = inputStack.top();								// Save last element to 2nd Temporary Variable
			inputStack.pop();															// Remove last element from stack

			inputStack.push(temporaryVariable1 * temporaryVariable2); // Multiplication of variables, and pushing them to the end of stack
		}

		//****** division ******
		else if (manualInput == "/")
		{
			double temporaryVariable1 = inputStack.top();								// Save last element to 1st Temporary Variable
			inputStack.pop();															// Remove last element from stack

			double temporaryVariable2 = inputStack.top();								// Save last element to 2nd Temporary Variable
			inputStack.pop();															// Remove last element from stack

			if (temporaryVariable2 != 0) // Preventing division by zero
			{
				inputStack.push(temporaryVariable1 / temporaryVariable2); // Divison of variables, and pushing them to the end of stack
			}

			else
			{
				std::cout << "Division by zero" << std::endl; // Information about division by zero
			}
		}

		//****** result ******
		else if (manualInput == "=")
		{
			if (inputStack.size() != 1) // If in stack there is more than one variable, expresion is invalid
			{
				std::cout << "Bad expression" << std::endl;
			}

			else
			{
				std::cout << inputStack.top() << std::endl;
				inputStack.pop(); // Flush input Stack
			}
		}

		//****** exit ******
		else if (manualInput == "x")
		{
			menu appMenu;
			appMenu.mainMenu();
		}

		//****** saving data into stack ******
		else
		{
			double inputToStackVariable;
			std::stringstream stringstreamObject(manualInput); // Constructs a stringstream object with an input sequence as content
			stringstreamObject >> inputToStackVariable; // 

			inputStack.push(inputToStackVariable);
		}
	}
}

void calculations::calcFromFile()
{
	std::cout << "Provide file location:";
	std::cin >> fileName;

	f_input.open(fileName, std::ios::in);	// opening file to read from
	std::string fileInput;

	int counter = 1;

	if (f_input.good())
	{
		while (f_input >> fileInput)
		{
			//****** multiplication ******
			if (fileInput == "*")
			{
				double temporaryVariable2 = inputStack.top();
				inputStack.pop();

				double temporaryVariable1 = inputStack.top();
				inputStack.pop();

				inputStack.push(temporaryVariable1*temporaryVariable2);
			}
		
			//****** addition ******
			else if (fileInput == "+")
			{
				double temporaryVariable2 = inputStack.top();
				inputStack.pop();

				double temporaryVariable1 = inputStack.top();
				inputStack.pop();

				inputStack.push(temporaryVariable1 + temporaryVariable2);
			}

			//****** subtraction ******
			else if (fileInput == "-")
			{
				double temporaryVariable2 = inputStack.top();
				inputStack.pop();

				double temporaryVariable1 = inputStack.top();
				inputStack.pop();

				inputStack.push(temporaryVariable1 - temporaryVariable2);
			}

			//****** division ******
			else if (fileInput == "/" || fileInput == "%") 
			{
				double temporaryVariable1 = inputStack.top();
				inputStack.pop();

				double temporaryVariable2 = inputStack.top();
				inputStack.pop();

				if (temporaryVariable2 != 0) // Preventing division by zero
				{
					inputStack.push(temporaryVariable1 / temporaryVariable2); // Divison of variables, and pushing them to the end of stack
				}

				else
				{
					std::cout << "Division by zero" << std::endl; // Information about division by zero
				}

			}

			//****** result ******
			else if (fileInput == "=" || fileInput == "\n")
			{
				std::cout << "Result number: " << counter << ": " << inputStack.top() << std::endl;
				inputStack.pop(); // Flush input Stack
				counter++;
			}

			else 
			{
				inputStack.push(atof(fileInput.c_str()));	// If it's temporaryVariable1 number
			}
		}
	}

	else std::cout << "Error reading file" << std::endl;

	f_input.close(); // Closing file
}

//-------------------------------------------------------------------------------------------------------------

void menu::mainMenu()
{
	std::cout << "Extended Reverse Polish Notation calculator\n"
		<< "Actions to perform: \n"
		<< "	1. Input your operations. \n "
		<< "	2. Load operations from file. \n"
		<< "\n	Press anything else to Exit.\n" << std::endl;

	std::cout << "What action do you want to perform?" << std::endl;
	std::cin >> choice;

	calculations calcFunct;

	if (choice == 1)
	{
		calcFunct.calcFromInput();
	}

	else if (choice == 2)
	{
		calcFunct.calcFromFile();
	}

	else
	{
		exit(0);
	}
}


//-------------------------------------------------------------------------------------------------------------